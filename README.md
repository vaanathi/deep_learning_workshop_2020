# Deep_learning_workshop_2021

The workshop would be split over three sessions of 2hrs duration over three consecutive weeks: 
Fundamentals of image analysis with DL, Intermediate session and project sessions.

## Installation of packages
Set up a pytorch environment with the necessary packages. For creating the environment, clone the above repository, open the command line prompt and cd into the cloned directory. Make sure you find the environment definition file dl_environment.yml in the folder.
<ul>
<li> Create a conda environment by entering the following command in the command prompt:
conda env create -f dl_environment.yml </li>
<li> Activate the environment using the following command:
conda activate pytorchenv </li>
<li> Now open the notebook using jupyter notebook & </li>
<li> And check if Check_packages.ipynb runs successfully. </li>
</ul>

You might also want to check out the pytorch tutorials on the basics of tensors and gradients before the workshop:
https://pytorch.org/tutorials/beginner/blitz/tensor_tutorial.html#sphx-glr-beginner-blitz-tensor-tutorial-py
https://pytorch.org/tutorials/beginner/blitz/autograd_tutorial.html#sphx-glr-beginner-blitz-autograd-tutorial-py

## Basics week material

### Dependencies
There shouldn't be anything too dependent on version numbers but the notebook was created using:
- Python 3.7.1 
- Torch 1.5.0

The tutorial material for the basics week consists of following contents:
- Loading input data and creating dataset and dataloaders
- Creating a model
- Defining the loss function 
- Define the training regime
- Predicting Results
- Effect of changing hyperparameters

The weights of a pre-trained model has been made available in week1_material/ named 'weights', in case it takes too long to train the model.

## Intermediate week material

### Dependencies
There shouldn't be anything too dependent on version numbers but the notebook was created using:
- Python 3.6.6
- Torch 1.4.0

### Tutorial 1: Classification of 2D brain slices into FLAIR and T1
Here are the contents of tutorial 1:

- Loading data from files and dynamic data augmentation within dataloaders
- Defining the transforms for data augmentation on your own
- Early stopping and checkpointing the model while training
- Learning hook functions
- Debugging and predicting features at intermediate layers
- Visualisation of features at a given layer using a t-stochastic neighbourhood embedding (t-SNE) plot
- Fine-tuning a pre-trained model, freezing layers

### Tutorial 2: Segmentation of Brain tumour lesions on FLAIR images
Here are the contents of tutorial 2:

- Loading data dynamically within dataloaders.
- Defining the 3D transforms for data augmentation on your own.
- VGG-FCN model based on VGG-16 architecture (mixture of static_method and nn.Sequential() to define the model).
- Using a learning rate scheduler
- Visualising the saliency of features using gradients

## Using the Colab versions of jupyter notebook files
## Mounting your drive to be used in Colab

Run the following code snippet in your colab notebook (already written in the colab version of the notebooks):

```
from google.colab import drive
drive.mount('/content/drive')
```

It will ask for your authentication. Click on the link, get the authentication code and paste it in the dialog box and press 'enter'.
If ypu had already mounted your drive, you will get a warning as below:

```
Drive already mounted at /content/drive; to attempt to forcibly remount, call drive.mount("/content/drive", force_remount=True).
```

Once the drive is mounted, create a folder called 'Week2_material' in 'My Drive', here is how I would load a file from it (already present in your colab notebook):

```
data_files = glob.glob('/content/drive/My Drive/Week2_material/Week2_data/*.npz')
```
